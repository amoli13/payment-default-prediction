# # Packages for Flask API
# from flask import Flask, request, render_template

# import pandas as pd
# import joblib

# app = Flask(__name__)

# @app.route('/')
# def hello_world():
#     return 'Hello World!'

# @app.route('/index')
# def file_input():
#     return render_template('index.html')

# @app.route('/predict', methods=['GET', 'POST'])
# def predictDefault():
#     if request.method == 'POST':
#         pred = pd.read_csv(request.files.get('predict'), sep=";").dropna(axis = 1, how = "any")
#         model = joblib.load("artifacts/bestmodel.pkl")
#         predictions = pred[["uuid"]].assign(default=model.predict(pred).tolist())  
        
#         return render_template('index.html', name="Predicted Default", data=predictions.to_html())
#     return render_template('index.html')
    


# #Starting a server on local host on port 3000  
# if __name__ == '__main__':
#     app.run(host='0.0.0.0', port=8080, debug=True)
    
# Packages for Flask API
from flask import Flask, request, render_template
from flask import json, Response

import pandas as pd
import joblib
from preprocessing import AccountStatusFeatureTransformer

def createJsonResponse(status, message, data, statusCode):
    js = ({'status':status,
           'message':message,
           'data':data})
    js = json.dumps(js)
    resp = Response(js, status=statusCode, mimetype='application/json')
    return resp

app = Flask(__name__)

@app.route('/')
def fileInput():
    return render_template('form.html')

@app.route('/predict', methods=['POST'])
def predictDefault():
    if 'predict' not in request.files:
            return createJsonResponse('error', 'No File Selected', [], 400)
    predict_file =  request.files.get('predict')
    if predict_file.filename != '':
        if predict_file.filename.rsplit('.', 1)[1].lower() != "csv":
            return createJsonResponse('error', 'Only CSV File Can Be Uploaded.', [], 400)
    if predict_file:
        try:
            pred = pd.read_csv(predict_file, sep=";").drop(columns=["default"])
            if pred.empty:
                return createJsonResponse('error', 'CSV File is Empty.', [], 400)
            model = joblib.load("artifacts/bestmodel.pkl")
            predictions = pred[["uuid"]].assign(default=model.predict(pred).tolist()) 
            return render_template('result.html', name="Predicted Probability of Default", data=predictions.to_html())
        except Exception as e:
            return createJsonResponse('error', 'Can Not Compute Predictions for Uploaded File', [], 200)
    else:
        return createJsonResponse('error', 'No File Selected', [], 400)
 
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
    
