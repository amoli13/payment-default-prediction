# Payment Default Prediction
| | |
| --- | --- |
| **Activity** | [![GitLab last commit](https://badgen.net/gitlab/last-commit/amoli13/payment-default-prediction/)](https://gitlab.com/amoli13/payment-default-prediction/-/commits) [![GitLab issues open](https://badgen.net/gitlab/open-issues/amoli13/payment-default-prediction)](https://gitlab.com/amoli13/payment-default-prediction/-/issues?sort=updated_desc&state=opened) [![GitLab issues closed](https://badgen.net/gitlab/closed-issues/amoli13/payment-default-prediction)](https://gitlab.com/amoli13/payment-default-prediction/-/issues?sort=updated_desc&state=closed) [![GitLab branch](https://badgen.net/gitlab/branches/amoli13/payment-default-prediction/)](https://gitlab.com/amoli13/payment-default-prediction/-/branches) [![GitLab mrs](https://badgen.net/gitlab/mrs/amoli13/payment-default-prediction/)](https://gitlab.com/amoli13/payment-default-prediction/-/merge_requests?scope=all&state=all) |
| **Tech Stack** | [![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)](https://gitlab.com/amoli13/payment-default-prediction) [![Flask](https://img.shields.io/badge/flask-%23000.svg?style=for-the-badge&logo=flask&logoColor=white)](https://gitlab.com/amoli13/payment-default-prediction/-/blob/main/flask_api/app/app.py)[![scikit-learn](https://img.shields.io/badge/scikit--learn-%23F7931E.svg?style=for-the-badge&logo=scikit-learn&logoColor=white)](https://gitlab.com/amoli13/payment-default-prediction/-/blob/main/default_prediction_report.ipynb) [![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white)](https://gitlab.com/amoli13/payment-default-prediction/-/blob/main/default_prediction_report.ipynb) [![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)](https://gitlab.com/amoli13/payment-default-prediction/-/blob/main/flask_api/Dockerfile) [![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white)](https://gitlab.com/amoli13/payment-default-prediction/-/tree/main#app-demo) [![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white)](https://gitlab.com/amoli13/payment-default-prediction/-/tree/main#app-demo) |
| **Notebook** | [`default_prediction_report.ipynb`](https://gitlab.com/amoli13/payment-default-prediction/-/blob/main/default_prediction_report.ipynb) |
| | |

## Goal
The dataset provided appears to be a series of account statements (99976) having some pre-processed features (43) representing a time frame. As the accurate timestamp is not provided for each statement, it is unlikely that the data points are indexed in ordered time. We have to predict the probability of each customer (account holder) defaulting their next payment based on the information of their account. The final working model is built into a web application that accepts the account data and display statement Id and probability of default for it. 

Web App for the model could be accessed at:  
http://ec2-3-15-39-200.us-east-2.compute.amazonaws.com:8080/

## App Demo
![App Demo](defaultPredictionAppWithFileInfoDemo.gif)

---
## Project Pipeline
Entire project build pipeline could be reproduced using the jupyter notebook [default_prediction_report.ipynb](https://gitlab.com/amoli13/payment-default-prediction/-/blob/main/default_prediction_report.ipynb). The notebook is also available as html web page in the repository.
1. **Interpreting Feature Space**: Building intuition about the data and understanding meaning behind each feature.
2. **Exploratory Data Analysis and Feature Engineering**:
- Analysing data in from the perspective of missing values, cardinality and variance.
- Performing data preprocessing techniques such as feature encoding, scaling, elimination and oversampling.
3. **Model Building, Optimisation and Hyperparameter Tuning**: As this is a binary classification problem I used eager learner Logistic Regression to perform supervised learning and generate description of our target function (default). As the dataset is highly imbalanced, the model we learn has high precision and low recall. To avoid causing poor customer experience, I have capped the postive class misclassification rate (False Positive Rate) at 4% so that only 4% of accounts are allowed to be mislabeled as defaulters. It is not the best estimator due to the underlying non linearity in the data but the linear decision boundry is able to classify imbalanced samples with 16.14% precision. Model is able to predict 34.2% of the defaults correctly. As the data we have is sparse, opting for better learning models such as SVM (RBF kernel), Random Forests or XGBOOST may significantly improve the accuracy of the model. 
5. **Deployment on AWS**: Model is exposed as web app using Flask. It is deployed as docker container inside the AWS EC2 instance. Nginx is configured as web server on the EC2 instance so that the app residing within the docker container can accept requests over the internet. 
![title](architecture.png)

## Testing the APP and Reproducing Results
- The app could be accessed at: 
http://ec2-3-15-39-200.us-east-2.compute.amazonaws.com:8080/
- Currently we have not configured TSL for our EC2 instance's public IP, so the app can not be accessed over HTTPS.
- Upload the _predict.csv_ file we created or any custom csv (semicolon delimiter) file containing data with the same header as our dataset. 
- One sample.csv file is created under the _data/_ folder of the repository to test the web app.
- Once sample.csv file is uploaded click on the predict button to view the predicted probability of the default for each data point.
- **NOTE:** Before running the notebook, add dataset.csv under the _data/_ folder of the repository.
