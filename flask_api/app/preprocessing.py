from sklearn.base import BaseEstimator, TransformerMixin

class AccountStatusFeatureTransformer(BaseEstimator, TransformerMixin):
        
    def fit(self, x, y=None):
        return self

    def transform(self, x):
        x["performance_score"] = x.sum(axis=1)
        x.drop(['account_worst_status_0_3m', 'account_worst_status_3_6m', 'account_worst_status_6_12m', 'account_worst_status_12_24m'], axis=1, inplace=True)
        return x	
